import {FETCH_CLIENTS} from "../actions";
import _ from 'lodash';

export default function(state = {}, action){
    switch (action.type) {
        case FETCH_CLIENTS:
            return _.mapKeys(action.payload.data, 'id');
        default:
            return state;
    }
}