import {DELETE_PRODUCT, EDIT_PRODUCT, FETCH_PRODUCTS} from "../actions";
import _ from 'lodash';

export default function (state = {}, action) {
    switch (action.type){
        case FETCH_PRODUCTS:
            return _.mapKeys(action.payload.data, "id");
        case EDIT_PRODUCT:
            return {...state, [action.payload.data.id]: action.payload.data};
        case DELETE_PRODUCT:
            return _.omit(state, action.payload);
        default:
            return state;
    }
}