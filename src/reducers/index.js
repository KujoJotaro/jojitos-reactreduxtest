import {combineReducers} from 'redux';
import ProductReducer from './reducer_products';
import ClientReducer from './reducer_clients';
import ProviderReducer from './reducer_providers'
import UserReducer from "./reducer_users";

import {reducer as formReducer} from 'redux-form';

const rootReducer = combineReducers({
    products: ProductReducer,
    clients: ClientReducer,
    users: UserReducer,
    providers: ProviderReducer,
    form: formReducer
});

export default rootReducer;
