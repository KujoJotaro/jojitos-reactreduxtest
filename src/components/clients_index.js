import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchClients} from '../actions';
import _ from 'lodash';

class ClientsIndex extends Component{
    componentDidMount(){
        this.props.fetchClients();
    }

    renderClient(){
        return _.map(this.props.clients, client => {
            if(client.id === 1 )
                return null;
            return(
                <tr key={client.id}>
                    <td>{client.nombre}</td>
                    <td>{client.apellidopaterno} {client.apellidomaterno}</td>
                    <td>{client.numerotelefono}</td>
                    <td>{client.sexo === 'M' ? 'Masculino': 'Femenino'}</td>
                    <td>{client.fiado === 'S' ? 'Si': 'No'}</td>
                    <td>
                        <Link to={`/clientes/editar/${client.id}`}>Editar</Link>
                    </td>
                </tr>
            );
        })
    }

    render(){
        return(
            <div className="col-md-12">
                <div className="col-md-12 hdr">
                    <div className="col-md-6">
                        <h3>Clientes</h3>
                    </div>
                    <div className=" pull-xs-right">
                        <Link className="btn btn-secondary" to="/clientes/agregar">
                            Nuevo Cliente
                        </Link>
                    </div>
                </div>
                <div className="col-md-12">
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Telefono</th>
                            <th>Sexo</th>
                            <th>Fiado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.renderClient()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {clients: state.clients};
}

export default connect(mapStateToProps, { fetchClients })(ClientsIndex);