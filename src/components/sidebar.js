import React from 'react';
import { Link } from 'react-router-dom';

export default function () {
    return (
        <div className="sidebar-wrapper">
            <ul className="sidebar-nav">
                <li className="sidebar-brand">
                    <Link to="/">React Redux</Link>
                </li>
                <li>
                    <Link to="/productos">Productos</Link>
                </li>
                <li>
                    <Link to="/clientes">Clientes</Link>
                </li>
                <li>
                    <Link to="/proveedores">Proveedores</Link>
                </li>
                <li>
                    <Link to="/usuarios">Usuarios</Link>
                </li>
            </ul>
        </div>
    );
}

