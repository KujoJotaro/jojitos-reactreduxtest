import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchProviders} from '../actions';
import _ from 'lodash';

class ProviderIndex extends Component {
    componentDidMount() {
        this.props.fetchProviders();
    }

    renderProviders() {
        return _.map(this.props.providers, provider => {
            return (
                <tr key={provider.id}>
                    <td>{provider.nombre}</td>
                    <td>{provider.numerotelefono}</td>
                </tr>
            );
        })
    }

    render() {
        return (
            <div className="col-md-12">
                <div className="col-md-12 hdr">
                    <div className="col-md-6">
                        <h3>Proveedores</h3>
                    </div>
                    <div className=" pull-xs-right">
                        <Link className="btn btn-secondary" to="/proveedores/agregar">
                            Nuevo Proveedor
                        </Link>
                    </div>
                </div>
                <div className="col-md-12">
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Telefono</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.renderProviders()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {providers: state.providers};
}

export default connect(mapStateToProps, {fetchProviders})(ProviderIndex);