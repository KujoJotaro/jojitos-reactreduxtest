import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import {fetchProduct, editProduct, deleteProduct} from "../actions";
import {Link} from 'react-router-dom';

class ProductEdit extends Component {
    componentDidMount() {
        if (!this.props.product) {
            const {id} = this.props.match.params;
            this.props.fetchProduct(id);
        }
        if (this.props.product) {
            this.handleInitialize();
        }
    }

    onDeleteClick() {
        this.props.deleteProduct(this.props.product.id, () => {
            this.props.history.push("/productos")
        });
    }

    handleInitialize() {
        if (this.props.product) {
            const initData = {
                "nombre": this.props.product.nombre,
                "descripcion": this.props.product.descripcion,
                "precioventa": this.props.product.precioventa,
                "preciocompra": this.props.product.preciocompra,
                "stock": this.props.product.stock,
                "stocklimite": this.props.product.stocklimite
            };

            this.props.initialize(initData);
        }
    }

    onSubmit(values) {
        this.props.editProduct(this.props.product.id, values, () => {
            this.props.history.push('/productos');
        });
    }

    renderField(field) {
        const {meta: {touched, error}} = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;

        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    className="form-control"
                    type={field.type}
                    {...field.input}
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    render() {
        const {product, handleSubmit, pristine, submitting} = this.props;

        if (!product)
            return <div>Cargando...</div>;

        return (
            <div key={product.id}>

                <h3>Editar Producto</h3>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="formadd">
                    <Field
                        label="Nombre"
                        type="text"
                        name="nombre"
                        component={this.renderField}
                    />

                    <Field
                        label="Descripcion"
                        type="text"
                        name="descripcion"
                        component={this.renderField}
                    />
                    <Field
                        label="Precio Venta"
                        type="number"
                        name="precioventa"
                        component={this.renderField}
                    />
                    <Field
                        label="Precio Compra"
                        type="number"
                        name="preciocompra"
                        component={this.renderField}
                    />

                    <Field
                        label="Stock"
                        type="number"
                        name="stock"
                        component={this.renderField}
                    />

                    <Field
                        label="Stock Limite"
                        type="number"
                        name="stocklimite"
                        component={this.renderField}
                    />
                    <button type="submit" className="btn btn-primary" disabled={pristine || submitting}>Guardar</button>
                    <Link className="btn btn-secondary espace" to="/productos">
                        Cancelar
                    </Link>
                    <button className="btn btn-danger espace pull-xs-right" onClick={this.onDeleteClick.bind(this)}>
                        Borrar Producto
                    </button>
                </form>


            </div>
        );
    }
}

function validate(values) {
    const errors = {};

    if (!values.nombre) {
        errors.nombre = "Ingrese un nombre";
    }

    if (!values.descripcion) {
        errors.descripcion = "Ingrese una descripcion";
    }

    if (!values.precioventa) {
        errors.precioventa = "Ingrese un precio de venta";
    }

    if (!values.preciocompra) {
        errors.preciocompra = "Ingrese un precio de compra";
    }

    if (!values.stock) {
        errors.stock = "Ingrese un stock";
    }

    if (!values.stocklimite) {
        errors.stocklimite = "Ingrese un limite de stock";
    }

    if (values.precioventa <= 0) {
        errors.precioventa = "El precio de venta tiene que ser mayor a 0";
    }

    if (values.preciocompra <= 0) {
        errors.preciocompra = "El precio de compra tiene que ser mayor a 0";
    }

    if (values.stock <= 0) {
        errors.stock = "El stock tiene que ser mayor a 0";
    }

    if (values.stocklimite <= 0) {
        errors.stocklimite = "El stock limite tiene que ser mayor a 0";
    }

    return errors;
}

function mapStateToProps({products}, ownProps) {
    return {product: products[ownProps.match.params.id]}
}

export default reduxForm({
    validate,
    form: 'ProductEditForm'
})(
    connect(mapStateToProps, {fetchProduct, editProduct, deleteProduct})(ProductEdit)
);