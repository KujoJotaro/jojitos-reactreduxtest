import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchUsers} from '../actions';
import _ from 'lodash';

class UserIndex extends Component {
    componentDidMount() {
        this.props.fetchUsers();
    }

    renderUsers() {
        return _.map(this.props.users, user => {
            return (
                <tr key={user.id}>
                    <td>{user.username}</td>
                    <td>{user.nombre} {user.apellidopaterno} {user.apellidomaterno}</td>
                    <td>{user.rut}</td>
                </tr>
            );
        })
    }

    render() {
        return (
            <div className="col-md-12">
                <div className="col-md-12 hdr">
                    <div className="col-md-6">
                        <h3>Usuarios</h3>
                    </div>
                    <div className=" pull-xs-right">
                        <Link className="btn btn-secondary" to="/usuarios/agregar">
                            Nuevo Usuario
                        </Link>
                    </div>
                </div>
                <div className="col-md-12">
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>Username</th>
                            <th>Nombre</th>
                            <th>Rut</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.renderUsers()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {users: state.users};
}

export default connect(mapStateToProps, {fetchUsers})(UserIndex);