import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {createProduct} from "../actions";

class ProductNew extends Component {
    renderField(field) {
        const {meta: {touched, error}} = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;

        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    className="form-control"
                    type={field.type}
                    {...field.input}
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    onSubmit(values) {
        this.props.createProduct(values, () => {
            this.props.history.push('/');
        });
    }

    render() {
        const {handleSubmit, pristine, submitting} = this.props;
        return (
            <div>
                <h3>Nuevo Producto</h3>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="formadd">
                    <Field
                        label="Nombre"
                        type="text"
                        name="nombre"
                        component={this.renderField}
                    />

                    <Field
                        label="Descripcion"
                        type="text"
                        name="descripcion"
                        component={this.renderField}
                    />
                    <Field
                        label="Precio Venta"
                        type="number"
                        name="precioventa"
                        component={this.renderField}
                    />
                    <Field
                        label="Precio Compra"
                        type="number"
                        name="preciocompra"
                        component={this.renderField}
                    />

                    <Field
                        label="Stock"
                        type="number"
                        name="stock"
                        component={this.renderField}
                    />

                    <Field
                        label="Stock Limite"
                        type="number"
                        name="stocklimite"
                        component={this.renderField}
                    />
                    <button type="submit" className="btn btn-primary" disabled={pristine || submitting}>Guardar</button>
                    <Link className="btn btn-danger espace" to="/productos">
                        Cancelar
                    </Link>
                </form>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};

    if (!values.nombre) {
        errors.nombre = "Ingrese un nombre";
    }

    if (!values.descripcion) {
        errors.descripcion = "Ingrese una descripcion";
    }

    if (!values.precioventa) {
        errors.precioventa = "Ingrese un precio de venta";
    }

    if (values.precioventa <= 0) {
        errors.precioventa = "El precio de venta tiene que ser mayor a 0";
    }

    if (values.preciocompra <= 0) {
        errors.preciocompra = "El precio de compra tiene que ser mayor a 0";
    }

    if (!values.preciocompra) {
        errors.preciocompra = "Ingrese un precio de compra";
    }

    if (values.stock <= 0) {
        errors.stock = "El stock tiene que ser mayor a 0";
    }

    if (!values.stock) {
        errors.stock = "Ingrese un stock";
    }

    if (values.stocklimite <= 0) {
        errors.stocklimite = "El stock limite tiene que ser mayor a 0";
    }

    if (values.stocklimite && values.stock > values.stocklimite) {
        errors.stock = "El stock no puede ser mayor al stock limite";
    }

    if (!values.stocklimite) {
        errors.stocklimite = "Ingrese un stock limite";
    }

    return errors;
}

export default reduxForm({
    validate,
    form: 'ProductNewForm'
})(
    connect(null, {createProduct})(ProductNew)
);
