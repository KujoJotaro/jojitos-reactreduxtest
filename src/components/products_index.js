import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchProducts} from '../actions';
import _ from 'lodash';

class ProductIndex extends Component {
    componentDidMount() {
        this.props.fetchProducts();
    }

    renderProduct() {
        // console.log(this.props.products);
        return _.map(this.props.products, product => {
            return (
                <tr key={product.id}>
                    <td>{product.nombre}</td>
                    <td>{product.descripcion}</td>
                    <td>{product.precioventa}</td>
                    <td>{product.stock}</td>
                    <td>
                        <Link to={`/productos/editar/${product.id}`}>Editar</Link>
                    </td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div className="col-md-12">
                <div className="col-md-12 hdr">
                    <div className="col-md-6">
                        <h3>Productos</h3>
                    </div>
                    <div className=" pull-xs-right">
                        <Link className="btn btn-secondary" to="/productos/agregar">
                            Nuevo Producto
                        </Link>
                    </div>
                </div>
                <div className="col-md-12">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descipcion</th>
                                <th>Precio</th>
                                <th>Stock</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderProduct()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {products: state.products};
}

export default connect(mapStateToProps, {fetchProducts})(ProductIndex);