import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {createClient} from "../actions";

class ClientNew extends Component{

    renderField(field) {
        const {meta: {touched, error}} = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;

        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    className="form-control"
                    type="text"
                    {...field.input}
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    onSubmit(values) {
        this.props.createClient(values, () => {
            this.props.history.push('/clientes');
        });
    }

    render() {
        const {handleSubmit, pristine, submitting} = this.props;

        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="formadd">
                <Field
                    label="Nombre"
                    name="nombre"
                    component={this.renderField}
                />

                <Field
                    label="Apellido materno"
                    name="apellidomaterno"
                    component={this.renderField}
                />

                <Field
                    label="Apellido paterno"
                    name="apellidopaterno"
                    component={this.renderField}
                />

                <Field
                    label="Telefono"
                    name="telefono"
                    component={this.renderField}
                />

                <Field
                    label="Sexo"
                    name="sexo"
                    component={this.renderField}
                />

                <Field
                    label="Fiado"
                    name="fiado"
                    component={this.renderField}
                />

                <button type="submit" className="btn btn-primary" disabled={pristine || submitting}>Guardar</button>
                <Link className="btn btn-danger espace" to="/clientes">
                    Cancelar
                </Link>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};

    if (!values.title) {
        errors.title = "Enter a title";
    }

    if (!values.categories) {
        errors.categories = "Enter some categories";
    }

    if (!values.content) {
        errors.content = "Enter some content";
    }

    return errors;
}

export default reduxForm({
    validate,
    form: 'ClientNewForm'
})(
    connect(null,{createClient})(ClientNew)
);