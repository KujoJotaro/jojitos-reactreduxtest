import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import promise from 'redux-promise';

import reducers from './reducers';

import ProductIndex from './components/products_index';
import ProductNew from './components/product_new';
import ProductEdit from './components/product_edit';

import ClientIndex from './components/clients_index';
import ClientNew from './components/client_new';
import ClientEdit from './components/client_edit';

import UserIndex from './components/users_index';

import ProviderIndex from './components/providers_index';

import Home from './components/home';
import SideBar from './components/sidebar';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <BrowserRouter>
            <div className="wrapper">
                <SideBar/>
                <div className="content">
                    <Switch>
                        <Route path="/productos/editar/:id"  component={ProductEdit}/>
                        <Route path="/clientes/editar/:id" component={ClientEdit}/>

                        <Route path="/productos/agregar" component={ProductNew}/>
                        <Route path="/clientes/agregar" component={ClientNew}/>

                        <Route path="/productos" component={ProductIndex}/>
                        <Route path="/proveedores" component={ProviderIndex}/>
                        <Route path="/usuarios" component={UserIndex}/>
                        <Route path="/clientes" component={ClientIndex}/>

                        <Route path="/" component={Home}/>
                    </Switch>
                </div>
            </div>
        </BrowserRouter>
    </Provider>
    , document.querySelector('.container'));



