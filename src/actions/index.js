import axios from 'axios';

export const FETCH_PRODUCTS = 'fetch_products';
export const EDIT_PRODUCT = 'edit_product';
export const FETCH_PRODUCT = 'fetch_product';
export const CREATE_PRODUCT = 'create_product';
export const DELETE_PRODUCT = 'delete_product';

export const FETCH_CLIENTS = 'fetch_clients';
export const CREATE_CLIENT = 'create_client';

export const FETCH_USERS = 'fetch_users';

export const FETCH_PROVIDERS = 'fetch_providers';

const ROOT_URL = 'http://localhost:1506/api';

export function deleteProduct(id, callback){
    axios.delete(`${ROOT_URL}/Productos/Eliminar/${id}`).then(callback());

    return {
        type:DELETE_PRODUCT,
        payload:id
    }
}

export function fetchProducts() {
    const request = axios.get(`${ROOT_URL}/Productos/Listar`);

    return {
        type: FETCH_PRODUCTS,
        payload: request
    };
}

export function createProduct(values, callback) {
    const request = axios.post(`${ROOT_URL}/Productos/Agregar`, values)
        .then(() => callback());

    return {
        type: CREATE_PRODUCT,
        payload: request
    }
}

export function fetchProduct(id) {
    const request = axios.get(`${ROOT_URL}/Productos/Buscar/${id}`)

    return {
        type: FETCH_PRODUCT,
        payload: request
    }
}

export function editProduct(id, values, callback) {
    const request = axios.put(`${ROOT_URL}/Productos/Actualizar/${id}`, values).then(() => callback());

    return {
        type: EDIT_PRODUCT,
        payload: request
    }

}


export function fetchClients() {
    const request = axios.get(`${ROOT_URL}/Clientes/Listar`);

    return {
        type: FETCH_CLIENTS,
        payload: request
    }
}

export function createClient(values, callback){
    const request = axios.post(`${ROOT_URL}/Clientes/Agregar`, values).then(() => callback());

    return{
        type: CREATE_CLIENT,
        payload: request
    }
}


export function fetchUsers() {
    const request = axios.get(`${ROOT_URL}/Usuarios/Listar`);

    return {
        type: FETCH_USERS,
        payload: request
    };
}


export function fetchProviders() {
    const request = axios.get(`${ROOT_URL}/Proveedores/Listar`);

    return {
        type: FETCH_PROVIDERS,
        payload: request
    };
}